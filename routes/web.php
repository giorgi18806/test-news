<?php

use App\Http\Controllers\Admin\CategoryController;
use App\Http\Controllers\Admin\HomeController;
use App\Http\Controllers\Admin\PostController;
use App\Http\Controllers\Admin\TagController;
use App\Http\Controllers\Admin\UserController;
use Illuminate\Support\Facades\Route;

Route::get('/', [\App\Http\Controllers\User\HomeController::class, 'index'])->name('user.home');

Route::get('/post/{post}', [\App\Http\Controllers\User\HomeController::class, 'show'])->name('user.post');
Route::get('/category/{category}', [\App\Http\Controllers\User\HomeController::class, 'showCategory'])->name('user.showCategory');


Route::get('/manager', [HomeController::class, 'index'])->name('admin.home');
Route::resource('/manager/posts', PostController::class);
Route::resource('/manager/categories', CategoryController::class);


