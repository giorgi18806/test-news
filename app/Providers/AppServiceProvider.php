<?php

namespace App\Providers;

use App\Http\View\Composers\CategoryComposer;
use App\Models\User\Category;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        View::composer([
            'user.includes.header',
            'user.pages.show',
            'admin.category.index',
            'admin.post.create',
            'admin.post.edit',

        ], CategoryComposer::class);
    }
}
