<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Models\User\Category;
use App\Models\User\Post;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index()
    {
        $posts = Post::all();
        return view('user.pages.index', compact('posts'));
    }

    public function show(Post $post)
    {
        return view('user.pages.show', compact('post'));
    }

    public function showCategory(Category $category)
    {
        $posts = Post::where('category_id', $category->id)->get();
        return view('user.pages.index', compact('posts'));
    }
}
