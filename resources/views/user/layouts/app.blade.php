@include('user.includes.head')

<body>

<!-- Navigation & Header-->
@include('user.includes.header')

<!-- Main Content -->
<div class="container">
    @yield('main-content')
</div>

<hr>

<!-- Footer -->
@include('user.includes.footer')

</body>

</html>
