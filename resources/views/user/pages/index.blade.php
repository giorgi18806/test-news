@extends('user.layouts.app')

@section('header-bg', asset('user/img/home-bg.jpg'))

@section('title', 'Giorgi\'s Blog')

@section('sub-title', 'The Road of Thousend Miles starts with First Step')

@section('main-content')
    <div class="row">
        <div class="col-lg-8 col-md-10 mx-auto">
            @foreach($posts as $post)
            <div class="post-preview">
                <a href="{{ route('user.post', $post) }}">
                    <h2 class="post-title">
                        {{ $post->title }}
                    </h2>
                    <h3 class="post-subtitle">
                        {{ $post->subtitle }}
                    </h3>
                </a>
                <p class="post-meta">Posted by
                    <a href="#">Giorgi18806</a>
                    on {{ $post->created_at }}</p>
            </div>
            @endforeach
        </div>
    </div>
@endsection
