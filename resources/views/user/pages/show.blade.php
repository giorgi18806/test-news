@extends('user.layouts.app')

@section('header-bg', asset('user/img/post-bg.jpg'))

@section('title', $post->title)

@section('sub-title', $post->subtitle)

@section('main-content')
    <article>
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-md-10 mx-auto">

                    <p>{!! htmlspecialchars_decode($post->body) !!}</p>

                </div>
            </div>
        </div>
    </article>
@endsection
