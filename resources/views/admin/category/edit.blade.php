@extends('admin.layouts.app')

@section('admin-content')
    <div class="card card-primary">
        <div class="card-header">
            <h3 class="card-title">Edit Category</h3>
        </div>
        <!-- /.card-header -->
        <!-- form start -->
        <form role="form" action="{{ route('categories.store') }}" method="POST">
            @csrf
            <div class="card-body">
                <div class="row">
                    <div class="offset-3 col-6">
                        <div class="form-group">
                            <label for="title">Category Title</label>
                            <input type="text" class="form-control" id="title" name="title" placeholder="Enter title" autofocus value="{{ $category->title }}">
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary">Update</button>
                            <a class="btn btn-warning" href="{{ route('categories.index') }}">Back</a>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
@endsection

