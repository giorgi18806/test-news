<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar user panel (optional) -->
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
            <h1>Test News</h1>
        </div>

        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-sidebar flex-column">
                <li class="nav-item ">
                    <a href="{{ route('categories.index') }}" class="nav-link {{ Request::is('manager/categories') ? 'active' : '' }}">
                        <i class="far fa-calendar"></i>
                        <p>Categories</p>
                    </a>
                </li>
                <li class="nav-item ">
                    <a href="{{ route('posts.index') }}" class="nav-link {{ Request::is('manager/posts') ? 'active' : '' }}">
                        <i class="far fa-copy"></i>
                        <p>Posts</p>
                    </a>
                </li>
            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>
