@include('admin.includes.head')
<body class="hold-transition sidebar-mini layout-fixed">
<div class="wrapper">

{{--    <!-- Navbar -->--}}
{{--    @include('admin.includes.navbar')--}}
{{--    <!-- /.navbar -->--}}

    <!-- Main Sidebar Container -->
    @include('admin.includes.sidebar')


    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        @yield('admin-content')
    </div>

   @include('admin.includes.footer')
</body>
</html>
