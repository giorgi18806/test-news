@extends('admin.layouts.app')

@section('admin-head')
    <link rel="stylesheet" href="{{ asset('admin/plugins/summernote/summernote-bs4.css') }}">
@endsection

@section('admin-content')
    <div class="card card-primary">
        <div class="card-header">
            <h3 class="card-title">Edit Post</h3>
        </div>
        @if(count($errors) > 0)
            @foreach($errors->all() as $error)
                <p class="alert alert-danger">{{ $error }}</p>
            @endforeach
        @endif
        <form role="form" action="{{ route('posts.update', $post) }}" method="POST">
            @csrf
            @method('PUT')
            <div class="card-body">
                <div class="row">
                        <div class="form-group col-4">
                            <label for="title">Post Title</label>
                            <input type="text" class="form-control" id="title" name="title" placeholder="Enter title" value="{{ $post->title }}">
                        </div>
                        <div class="form-group col-4">
                            <label for="sub_title">Post Sub Title</label>
                            <input type="text" class="form-control" id="sub_title" name="sub_title"
                                   placeholder="Enter Sub title" value="{{ $post->sub_title }}">
                        </div>
                        <div class="form-group col-4">
                            <label for="category_id">Post Sub Title</label>
                            <select class="form-control" name="category_id" id="category_id">
                                @foreach($categories as $category)
                                    <option value="{{ $category->id }}" @if($category->id == $post->category_id) selected @endif>{{ $category->title }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                <div class="mb-3">
                    <textarea class="textarea" placeholder="Place some text here" name="body" rows="20" style="width: 100%; height: 500px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;">{{ $post->body }}</textarea>
                </div>
            </div>


            <div class="card-footer">
                <button type="submit" class="btn btn-primary">Update</button>
                <a href="{{ route('posts.index') }}" class="btn btn-warning">Back</a>
            </div>
        </form>
    </div>
@endsection

@section('admin-footer')

    <!-- AdminLTE for demo purposes -->
{{--    <script src="{{ asset('../../dist/js/demo.js') }}"></script>--}}
    <!-- Summernote -->
    <script src="{{ asset('admin/plugins/summernote/summernote-bs4.min.js') }}"></script>
    <script>
        $(function () {
            // Summernote
            $('.textarea').summernote()
        })
    </script>
@endsection
